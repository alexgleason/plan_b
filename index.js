/******************************************************************************

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.

******************************************************************************/

var sdk = require("matrix-js-sdk");

// Authorize client
var userId = process.env['MATRIX_USER'];
var matrixClient = sdk.createClient({
    baseUrl: process.env['MATRIX_SERVER_URL'],
    accessToken: process.env['MATRIX_ACCESS_TOKEN'],
    userId: userId
});

// Get whitelist of users in csv, eg: per1@matrix.org,per2@matrix.org
var whitelist = process.env['MATRIX_USER_WHITELIST'].split(",");
if (!whitelist.includes(userId)) {
  whitelist.push(userId);
}
console.log("Whitelisted users: %s", whitelist);

// Kick all users for the given roomId
function kickAll(roomId) {
  var room = matrixClient.getRoom(roomId);
  if (!room) {
    return;
  }
  var members = room.getJoinedMembers().concat(room.getMembersWithMembership("invite"));
  members.forEach(function(member) {
    if (whitelist.includes(member.userId)) {
      return // Skip whitelisted members
    }
    // Obliterate!
    matrixClient.kick(roomId, member.userId, "", function() {
      console.log("Kicked: %s", member.userId);
    });
  });
}

// Automatically join rooms when invited
matrixClient.on("RoomMember.membership", function(event, member) {
  if (member.membership === "invite" && member.userId === userId) {
    matrixClient.joinRoom(member.roomId).done(function() {
      console.log("Auto-joined %s", member.roomId);
    });
  }
});

// Kick whenenver the members list updates
matrixClient.on("RoomState.members", function(event, state, member) {
  if (member.membership == "join" || member.membership == "invite") {
    kickAll(state.roomId);
  }
});

// Kick whenever a power level updates
matrixClient.on("RoomMember.powerLevel", function(event, member) {
  kickAll(member.roomId);
});

// Obliterate incoming messages
matrixClient.once('sync', function(state, prevState) {
  if (state === 'PREPARED') {
    matrixClient.on("Room.timeline", function(event, room, toStartOfTimeline) {
      if (toStartOfTimeline || event.getSender() === userId) {
        return // Ignore own messages
      }
      if (whitelist.includes(event.getSender())) {
        return // Ignore whitelisted users
      }
      if (event.getType() !== "m.room.message") {
        return // Only respond to normal messages
      }
      // Obliterate!
      matrixClient.redactEvent(room.roomId, event.getId(), function() {
        console.log("Redacted event: %s", event.getId());
      });
    })
  }
})

// Start
matrixClient.startClient();
